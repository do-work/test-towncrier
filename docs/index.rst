.. Towncrier Test documentation master file, created by
   sphinx-quickstart on Mon Jul  3 13:43:43 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Towncrier Test's documentation!
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   release-notes.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
