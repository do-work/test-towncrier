Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog <https://keepachangelog.com/en/1.0.0/>`_, and this project adheres to `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`_.

This project uses `towncrier <https://towncrier.readthedocs.io/>`_ and the changes for the upcoming release can be found in `the repo <https://gitlab.com/do-work/test-towncrier/-/tree/main/releasenotes/notes>`_.

.. towncrier release notes start

0.2.2 (2023-07-03)
==================

Bugfixes
--------

- Another Fix 10 (`#10 <https://gitlab.com/do-work/test-towncrier/-/issues/10>`_)


0.2.1 (2023-07-03)
==================

Bugfixes
--------

- That's a very nice resolved bug.
  And it's very important.
  You need to write a bit more.
  Yeah. (`#2 <https://gitlab.com/do-work/test-towncrier/-/issues/2>`_)


0.2 (2023-07-03)
================

Features
--------

- Another Feature 1 (`#1 <https://gitlab.com/do-work/test-towncrier/-/issues/1>`_)


Bugfixes
--------

- Another Fix 1 (`#1 <https://gitlab.com/do-work/test-towncrier/-/issues/1>`_)
- Another Fix 3 (`#3 <https://gitlab.com/do-work/test-towncrier/-/issues/3>`_)


0.1 (2023-07-03)
================

Features
--------

- Feature 1 (`#1 <https://gitlab.com/do-work/test-towncrier/-/issues/1>`_)
- Feature 4 (`#4 <https://gitlab.com/do-work/test-towncrier/-/issues/4>`_)


Bugfixes
--------

- Fix 1 (`#1 <https://gitlab.com/do-work/test-towncrier/-/issues/1>`_)
- Fix 2 (`#2 <https://gitlab.com/do-work/test-towncrier/-/issues/2>`_)
- Fix 3 (`#3 <https://gitlab.com/do-work/test-towncrier/-/issues/3>`_)
